<?php

function instrument($label, $var)
{
    echo $label;
    echo '<pre>';
    print_r($var);
    echo '</pre>';
}

define('LIBDIR', 'system/libraries/');
define('DATADIR', 'app/data/');
$dsn = 'sqlite:' . DATADIR . 'pits.sq3';

$d = explode(':', $dsn);
if (file_exists($d[1])) {
    die('<h2>Database exists and cannot be reset.</h2>');
    unlink($d[1]);
}

include LIBDIR . 'database.lib.php';
$db = new database($dsn);

$db->begin();

$sql = <<<SQL
CREATE TABLE useraux (
id integer primary key, 
login varchar(30) not null, 
nonce varchar(255) not null, 
name varchar(50) not null, 
specialty varchar(50), 
languages varchar(50), 
startdt date not null, 
enddt date)
SQL;
$db->query($sql);

$sql = <<<SQL
CREATE TABLE projects (
id integer primary key autoincrement,
name varchar(20) not null, 
descrip varchar(50) not null, 
language varchar(20), 
responsible integer references useraux(id),
notes varchar(512)) 
SQL;
$db->query($sql);

$sql = <<<SQL
CREATE TABLE issues ( 
id integer primary key autoincrement, 
pid integer references projects(id) not null,
version varchar(30) default '',
platform varchar(30) default '',
descrip varchar(50) not null,
notes varchar(255),
severity integer default 0 not null,
priority integer default 0 not null,
status integer default 1 not null,
reportedby integer references useraux(id) not null,
reportdate integer not null,
compdate integer)
SQL;
$db->query($sql);

$sql = <<<SQL
CREATE TABLE log (
id integer primary key autoincrement,
uid integer references useraux(id) not null,
event varchar(50) not null,
timestamp integer not null)
SQL;
$db->query($sql);

$sql = <<<SQL
CREATE TABLE templateh (
id integer primary key autoincrement,
descrip varchar(30) not null)
SQL;
$db->query($sql);

$sql = <<<SQL
CREATE TABLE templated (
id integer primary key autoincrement,
tmplid integer references templateh(id),
tgtord integer not null,
tgttext varchar(50) not null)
SQL;
$db->query($sql);

$sql = <<<SQL
CREATE TABLE rformh (
id integer primary key autoincrement,
tmplid integer references templateh(id),
pid integer references projects(id),
descrip varchar(30) not null,
startdate integer not null,
compdate integer,
status char(1) default ' ') /* ' ' = I/P, 'S' = skipped, 'C' = completed */
SQL;
$db->query($sql);

$sql = <<<SQL
CREATE TABLE rformd (
id integer primary key autoincrement,
rformid integer references rformh(id),
tgtord integer not null,
tgttext varchar(50) not null,
compdate integer,
compedby integer references useraux(id))
SQL;
$db->query($sql);

$db->commit();

