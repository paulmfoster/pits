<?php

define('SEV_BLOCK', 5);
define('SEV_CRASH', 4);
define('SEV_FAIL', 3);
define('SEV_DESIGN', 2);
define('SEV_FEATURE', 1);

$sev_slugs = [
    SEV_BLOCK => 'BLOCKING',
    SEV_CRASH => 'CRASH',
    SEV_FAIL => 'FAILS',
    SEV_DESIGN => 'DESIGN FLAW',
    SEV_FEATURE => 'FEATURE REQUEST'
];

$sev_explans = [
    SEV_BLOCK => "Software won't compile or parse.",
    SEV_CRASH => "Software crashes.",
    SEV_FAIL => "Software runs, but fails to do what it's supposed to do, or does something different.",
    SEV_DESIGN => "Works as designedd, but design is wrong",
    SEV_FEATURE => "Feature request."
];

define('PRI_URGENT', 4);
define('PRI_HIGH', 3);
define('PRI_MEDIUM', 2);
define('PRI_LOW', 1);

$pri_slugs = [
    PRI_URGENT => 'URGENT/CRITICAL',
    PRI_HIGH => 'HIGH',
    PRI_MEDIUM => 'MEDIUM',
    PRI_LOW => 'LOW'
];

define('STA_NEW', 1);
define('STA_ACK', 2);
define('STA_CNF', 3);
define('STA_ASS', 4);
define('STA_RSLVD', 5);
define('STA_CLSD', 6);
define('STA_FXD', 7);
define('STA_DUP', 8);
define('STA_WONT', 9);
define('STA_NCR', 10);

$status_slugs = [
    STA_NEW => 'NEW',
    STA_ACK => 'ACKNOWLEDGED',
    STA_CNF => 'CONFIRMED',
    STA_ASS => 'ASSIGNED',
    STA_RSLVD => 'RESOLVED',
    STA_CLSD => 'CLOSED',
    STA_FXD => 'FIXED',
    STA_DUP => 'DUPLICATE',
    STA_WONT => 'WONTFIX',
    STA_NCR => 'NOCHANGE'
];

define('PGMR_LEVEL', 32);

$nav_links = [
    'Projects' => [
        'List Projects' => 'index.php?url=pjt/list',
        'Add Project' => 'index.php?url=pjt/add'
    ],
    'Deployment Templates' => [
        'List Templates' => 'index.php?url=tmpl/list',
        'Add Template' => 'index.php?url=tmpl/add'
    ],
    'Log' => [
        'Show Log' => 'index.php?url=log/show',
        'Clear Log' => 'index.php?url=log/clear'
    ],
    'Users' => [
        'Log In' => 'index.php?url=usr/login',
        'Log Out' => 'index.php?url=usr/logout',
        'List Users' => 'index.php?url=usr/list',
        'Add User' => 'index.php?url=usr/add'
    ],
    'Help' => [
        'Help' => 'index.php?url=welcome/help'
    ]
];

// This code is common to many/most controllers
ini_set('session.gc_maxlifetime', 2592000);
ini_set('session.cookie_lifetime', 2592000);
session_set_cookie_params(2592000);
session_start();
$cfg = parse_ini_file(CFGDIR . 'config.ini');
$dsn = explode(':', $cfg['dsn']);
if (!file_exists($dsn[1])) {
    $db = make_tables($cfg['dsn'], APPDIR . 'coldstart.sqlite');
}
else {
    $db = load('database', $cfg['dsn']);
}

load('pdate');

$exist = file_exists(DATADIR . 'users.sq3') ? TRUE : FALSE;
$user = load('user');
if (!$exist) {
    $u = $user->last_user();
    $dt = pdate::toiso(pdate::now());
    $u['startdt'] = $dt;
    $pu = $db->prepare('useraux', $u);
    $db->insert('useraux', $pu);
}
$current_user = $user->get_current_user();

load('errors');
load('messages');
$form = load('form');
$nav = load('navigation');
$nav->init('H', $nav_links);

function access($level, $url)
{
	global $user;

	$okay = $user->access($level);
	if (!$okay) {
		header('Location: ' . $url);
		exit();
		return FALSE;
	}
}

function show_project()
{
    $pid = $_SESSION['pid'] ?? NULL;
    if (is_null($pid)) {
        redirect('index.php');
    }
    redirect('index.php?url=pjt/show/' . $pid);
}

/* Not used but saved for future use
function get_seconds($duration)
{
    $secs = 0;

    $num = 0;
    $len = strlen($duration);
    for ($i = 0; $i < $len; $i++) {
        if (is_numeric($duration[$i])) {
            $num = ($num * 10) + (int) $duration[$i];
        }
        else {
            switch ($duration[$i]) {
            case 'd':
                $secs += $num * 86400;
                break;
            case 'h':
                $secs += $num * 3600;
                break;
            case 'm':
                $secs += $num * 60;
                break;
            }
            $num = 0;
        }
    }
    return $secs;
}

function duration2seconds($secs)
{
    $str = '';

    $days = intdiv($secs, 86400);
    $rmdr = $secs % 86400;

    $hours = intdiv($rmdr, 3600);
    $rmdr = $secs % 3600;

    $minutes = intdiv($rmdr, 60);

    if ($days != 0) {
        $str .= $days . 'd';
    }
    if ($hours != 0) {
        $str .= $hours . 'h';
    }
    if ($minutes != 0) {
        $str .= $minutes . 'm';
    }

    return $str;
}
 */

function dblog($type, $table)
{
    global $db, $current_user;
    $rec = [
        'uid' => $current_user['id'],
        'event' => strtoupper($type) . ' ON ' . $table . ' TABLE',
        'timestamp' => time()
    ];

    $db->insert('log', $rec);
}

