<?php

class projects
{
    function __construct()
    {
        global $db, $current_user;
        $this->db = $db;
        $this->current_user = $current_user;
    }

    function by_id($pid, $history = FALSE)
    {
        // project itself
        $sql = "SELECT p.*, u.login AS login FROM projects AS p JOIN useraux AS u ON u.id = p.responsible WHERE p.id = $pid";
        $pjts = $this->db->query($sql)->fetch();
        if ($pjts == FALSE) {
            return FALSE;
        }

        // issues
        if ($history) {
            $sql = "select i.*, a.login as login, p.name as project_name from issues as i join useraux as a on (a.id = i.reportedby) join projects as p on (p.id = i.pid) where pid = $pid";
        }
        else {
            $sql = "select i.*, a.login as login, p.name as project_name from issues as i join useraux as a on (a.id = i.reportedby) join projects as p on (p.id = i.pid) where pid = $pid and compdate IS NULL";
        }

        $isses = $this->db->query($sql)->fetch_all();

        // deployments
        if ($history) {
            $sql = "SELECT h.*, p.name AS project_name, p.descrip AS project_descrip FROM rformh AS h JOIN projects AS p ON p.id = h.pid WHERE pid = $pid";
        }
        else {
            $sql = "SELECT h.*, p.name AS project_name, p.descrip AS project_descrip FROM rformh AS h JOIN projects AS p ON p.id = h.pid WHERE pid = $pid AND status != 'C'";
        }
        $progs = $this->db->query($sql)->fetch_all();
        $max = ($progs == FALSE) ? 0 : count($progs);
        for ($i = 0; $i < $max; $i++) {
            switch ($progs[$i]['status']) {
            case ' ':
                $progs[$i]['x_status'] = 'I/P';
                break;
            case 'S':
                $progs[$i]['x_status'] = 'Skipped';
                break;
            case 'C':
                $progs[$i]['x_status'] = 'Completed';
                break;
            }

            // detail
            $sql = "SELECT * FROM rformd WHERE rformid = {$progs[$i]['id']} ORDER BY tgtord";
            $progs[$i]['detail'] = $this->db->query($sql)->fetch_all();
            $dmax = count($progs[$i]['detail']);
            for ($j = 0; $j < $dmax; $j++) {
                if (!is_null($progs[$i]['detail'][$j]['compedby'])) {
                    $sql = "SELECT login FROM useraux WHERE id = {$progs[$i]['detail'][$j]['compedby']}";
                    $login = $this->db->query($sql)->fetch();
                    $progs[$i]['detail'][$j]['login'] = $login['login'];
                }
                else {
                    $progs[$i]['detail'][$j]['login'] = '';
                }
            }
        }

        return [$pjts, $isses, $progs];
    }

    function brief($pid)
    {
        $sql = "SELECT p.*, u.login AS login FROM projects AS p JOIN useraux AS u ON u.id = p.responsible WHERE p.id = $pid";
        $results = $this->db->query($sql)->fetch();
        if ($results == FALSE) {
            return FALSE;
        }
        return $results;
    }

    function by_name($name)
    {
        $sql = "SELECT p.*, u.login AS login FROM projects AS p JOIN useraux AS u ON u.id = p.responsible WHERE p.name = '$name'";
        $results = $this->db->query($sql)->fetch();
        if ($results == FALSE) {
            return FALSE;
        }
        return $results;
    }

    function add($post)
    {
        $results = $this->by_name($post['name']);
        if ($results != FALSE) {
            // project name already in use
            return FALSE;
        }

        $prec = $this->db->prepare('projects', $post);
        $this->db->insert('projects', $prec); 
        dblog('insert', 'projects');

        return TRUE;
    }

    function list()
    {
        $sql = "SELECT p.*, u.login AS login FROM projects AS p JOIN useraux AS u ON u.id = p.responsible ORDER BY name COLLATE NOCASE";
        $results = $this->db->query($sql)->fetch_all();
        return $results;
    }

    function update($post)
    {
        $prec = $this->db->prepare('projects', $post);
        $result = $this->db->update('projects', $prec, "id = {$post['id']}");
        dblog('update', 'projects');
        return ($result == FALSE) ? FALSE : TRUE; 
    }

    function delete($pid)
    {
        $this->db->begin();
        $this->db->delete('issues', "pid = $pid");
        dblog('delete', 'issues');

        // handle rform detail first to avoid referential problems
        $sql = "SELECT id FROM rformh WHERE pid = $pid";
        $result = $this->db->query($sql)->fetch();
        if ($result != FALSE) {
            $this->db->delete('rformd', "rformid = {$result['id']}");
            dblog('delete', 'rformd');
        }

        $this->db->delete('rformh', "pid = $pid");
        dblog('delete', 'rformh');
        $this->db->delete('projects', "id = $pid");
        dblog('delete', 'projects');
        $this->db->commit();
        return TRUE;
    }

}
