<?php

class rforms
{
    function __construct()
    {
        global $db, $current_user;
        $this->db = $db;
        $this->current_user = $current_user;
    }

    function list($pid = NULL)
    {
        $sql = "SELECT h.*, p.name AS project_name, p.descrip AS project_descrip FROM rformh AS h JOIN projects AS p ON p.id = h.pid WHERE h.pid = $pid";
        $recs = $this->db->query($sql)->fetch_all();
        $max = ($recs == FALSE) ? 0 : count($recs);
        for ($i = 0; $i < $max; $i++) {
            switch ($recs[$i]['status']) {
            case ' ':
                $recs[$i]['x_status'] = 'I/P';
                break;
            case 'S':
                $recs[$i]['x_status'] = 'Skipped';
                break;
            case 'C':
                $recs[$i]['x_status'] = 'Completed';
                break;
            }
        }
        return $recs;
    }

    function add($post)
    {
        $sql = "SELECT * FROM templateh WHERE id = {$post['id']}";
        $head = $this->db->query($sql)->fetch();
        $sql = "SELECT * FROM templated WHERE tmplid = {$post['id']}";
        $detail = $this->db->query($sql)->fetch_all();

        $this->db->begin();
        // rformh
        $h = [
            'tmplid' => $post['id'],
            'pid' => $post['pid'],
            'descrip' => (isset($post['descrip']) && !empty($post['descrip'])) ? $post['descrip'] : $head['descrip'],
            'startdate' => time()
        ];
        $this->db->insert('rformh', $h);
        dblog('insert', 'rformh');
        $lastid = $this->db->lastid('rformh');

        // rformd
        foreach ($detail as $dtl) {
            $d = [
                'rformid' => $lastid,
                'tgtord' => $dtl['tgtord'],
                'tgttext' => $dtl['tgttext']
            ];
            $this->db->insert('rformd', $d);
            dblog('insert', 'rformd');
        }

        $this->db->commit();        
        return $lastid;
    }

    function get($id)
    {
        $sql = "SELECT h.*, r.descrip AS rfdescrip, p.name AS project_name, p.descrip AS project_descrip FROM rformh AS h JOIN templateh AS r ON r.id = h.tmplid JOIN projects AS p ON p.id = h.pid WHERE h.id = $id";
        $head = $this->db->query($sql)->fetch();

        switch ($head['status']) {
        case ' ':
            $head['x_status'] = 'I/P';
            break;
        case 'S':
            $head['x_status'] = 'Skipped';
            break;
        case 'C':
            $head['x_status'] = 'Completed';
            break;
        }

        $sql = "SELECT d.* FROM rformd AS d WHERE rformid = $id";
        $detail = $this->db->query($sql)->fetch_all();
        $max = count($detail);
        for ($i = 0; $i < $max; $i++) {
            if (is_numeric($detail[$i]['compedby'])) {
                $sql = "SELECT login FROM useraux WHERE id = {$detail[$i]['compedby']}";
                $login_rec = $this->db->query($sql)->fetch();
                if ($login_rec != FALSE) {
                    $detail[$i]['login'] = $login_rec['login'];
                }
                else {
                    // this shouldn't happen
                    $detail[$i]['login'] = '';
                }
            }
            else {
                $detail[$i]['login'] = '';
            }
        }

        return ['head' => $head, 'detail' => $detail];
    }

    function update($post)
    {
        foreach ($post as $key => $val) {
            if (strpos($key, 'tgtord_') === 0) {
                // get the target ID
                $n = substr($key, 7);
                $this->db->update('rformd', ['tgtord' => $val], "id = $n");
            }
            if (strpos($key, 'tgttext_') === 0) {
                // get the target ID
                $n = substr($key, 8);
                $this->db->update('rformd', ['tgttext' => $val], "id = $n");
            }
            if (strpos($key, 'comped_') === 0) {
                // get the target ID
                $n = substr($key, 7);
                $rec = [
                    'compdate' => time(),
                    'compedby' => $this->current_user['id']
                ];
                $this->db->update('rformd', $rec, "id = $n");
            }
        }
    }

    function delete($rformid)
    {
        $this->db->delete('rformd', "rformid = $rformid");
        dblog('delete', 'rformd');
        $this->db->delete('rformh', "id = $rformid");
        dblog('delete', 'rformh');
    }

    function complete($rformid)
    {
        $sql = "SELECT pid, status FROM rformh WHERE id = $rformid";
        $r = $this->db->query($sql)->fetch();
        if ($r['status'] != 'C') {
            $this->db->update('rformh', ['compdate' => time(), 'status' => 'C'], "id = $rformid");
            dblog('update', 'rformh');
        }
        return $r['pid'];
    }
}
