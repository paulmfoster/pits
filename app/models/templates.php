<?php

class templates
{
    function __construct()
    {
        global $db, $current_user;
        $this->db = $db;
        $this->current_user = $current_user;
    }

    function add($post)
    {
        // header
        $this->db->insert('templateh', ['descrip' => $post['descrip']]);
        dblog('insert', 'templateh');
        $id = $this->db->lastid('templateh');

        // detail
        $n = 10;
        $lines = explode("\r\n", $post['targets']);
        foreach ($lines as $line) {
            if (!empty(trim($line))) {
                $r = [
                    'tmplid' => $id,
                    'tgtord' => $n,
                    'tgttext' => $line
                ];
                $this->db->insert('templated', $r);
                dblog('insert', 'templated');
                $n += 10;
            }
        }
        return $id;
    }

    function get($id)
    {
        $sql = "SELECT d.*, h.descrip FROM templated AS d JOIN templateh AS h ON h.id = d.tmplid WHERE d.tmplid = $id ORDER BY d.tgtord";
        $recs = $this->db->query($sql)->fetch_all();
        return $recs;
    }

    function list()
    {
        $sql = "SELECT * FROM templateh ORDER BY descrip";
        $recs = $this->db->query($sql)->fetch_all();
        return $recs;
    }

    function update($post)
    {
        foreach ($post as $key => $val) {
            if ($key == 'descrip') {
                $this->db->update('templateh', ['descrip' => $post['descrip']], "id = {$post['id']}");
                dblog('update', 'templateh');
            }
            else {
                if (strpos($key, 'tgtord_') === 0) {
                    $id = substr($key, 7);
                    $this->db->update('templated', ['tgtord' => $val], "id = $id");
                    dblog('update', 'templated');
                }
                elseif (strpos($key, 'tgttext_') === 0) {
                    $id = substr($key, 8);
                    $this->db->update('templated', ['tgttext' => $val], "id = $id");
                    dblog('update', 'templated');
                }
            } // else
        } // foreach
    }

    function delete($template_id)
    {
        // are there any of this template actually in use?
        $sql = "SELECT id FROM rformh WHERE tmplid = $template_id";
        $recs = $this->db->query($sql)->fetch_all();
        if ($recs !== FALSE) {
            return FALSE;
        }
        // delete detail first, so as to not violate key constraints
        $this->db->delete('templated', "tmplid = $template_id");
        dblog('delete', 'templated');
        $this->db->delete('templateh', "id = $template_id");
        dblog('delete', 'templateh');
        return TRUE;
    }

}
