<?php

class issues
{
    function __construct()
    {
        global $db, $user, $current_user;

        $this->db = $db;
        $this->user = $user;
        $this->current_user = $current_user;
    }

    function add($post)
    {
        $prec = $this->db->prepare('issues', $post);
        $this->db->insert('issues', $prec);
        dblog('insert', 'issues');
        $lid = $this->db->lastid('issues');
        return ($lid != FALSE) ? $lid : FALSE;
    }

    function list($pid)
    {
        $sql = "SELECT i.*, a.login as login, p.name as project_name FROM issues AS i JOIN useraux as a on (a.id = i.reportedby) JOIN projects AS p ON (p.id = i.pid)";
        $isses = $this->db->query($sql)->fetch_all();
        return $isses;
    }

    function by_id($id)
    {
        $sql = "SELECT i.*, a.login as login, p.name as project_name FROM issues AS i JOIN useraux as a on (a.id = i.reportedby) JOIN projects AS p ON (p.id = i.pid) WHERE i.id = $id";
        $rec = $this->db->query($sql)->fetch();
        return $rec;
    }

    function update($post)
    {
        // supply closed date if status >= closed
        if ($post['status'] >= STA_CLSD) {
            $post['compdate'] = time();
        }
        $prec = $this->db->prepare('issues', $post);
        $this->db->update('issues', $prec, "id = {$post['id']}");
        dblog('update', 'issues');
    }

    function delete($id)
    {
        $this->db->delete('issues', "id = $id");
        dblog('delete', 'issues');
    }

    function complete($id)
    {
        $compdate = time();
        $this->db->update('issues', ['compdate' => $compdate], "id = $id");
        dblog('update', 'issues');
    }
}

