<?php

class users
{
    function __construct()
    {
        global $db, $user, $current_user;

        $this->db = $db;
        $this->user = $user;
        $this->current_user = $current_user;
    }

    function add($post)
    {
        // add to user database first
        dblog('insert', 'users');
        if (!$this->user->add_user($post)) {
            return FALSE;
        }

        // get ID for useraux table:w
        $u = $this->user->get_user_by_login($post['login']);
        $id = $u['id'];

        // add to pits database
        $urec = [
            'id' => $id,
            'login' => $post['login'],
            'name' => $post['name'],
            'nonce' => $u['nonce'],
            'specialty' => $post['specialty'] ?? NULL,
            'languages' => $post['languages'] ?? NULL,
            'startdt' => pdate::toiso(pdate::now())
        ];
        dblog('insert', 'useraux');
        if ($this->db->insert('useraux', $urec)) {
            emsg('S', "User {$post['name']} successfully added.");
            return TRUE;
        }
        else {
            emsg('F', "Unable to add user {$post['name']}. See your database administrator.");
            return FALSE;
        }
    }

    function login($post)
    {
        return $this->user->login($post);
    }

    function list()
    {
        $recs = [];
        $users = $this->user->get_user_list();
        $sql = "SELECT * FROM useraux";
        $aux = $this->db->query($sql)->fetch_all();
        foreach ($users as $u) {
            $found = FALSE;
            foreach ($aux as $a) {
                if ($a['id'] == $u['id']) {
                    $found = TRUE;
                    $recs[] = [
                        'id' => $u['id'],
                        'login' => $u['login'],
                        'email' => $u['email'],
                        'name' => $u['name'],
                        'nonce' => $u['nonce'],
                        'level' => $u['level'],
                        'specialty' => $a['specialty'],
                        'languages' => $a['languages'],
                        'startdt' => $a['startdt'],
                        'enddt' => $a['enddt']
                    ];
                    break;
                }
            }
            if (!$found) {
                // likely an admin
                $recs[] = [
                    'id' => $u['id'],
                    'login' => $u['login'],
                    'email' => $u['email'],
                    'name' => $u['name'],
                    'nonce' => $u['nonce'],
                    'level' => $u['level'],
                    'specialty' => '',
                    'languages' => '',
                    'startdt' => '',
                    'enddt' => ''
                ];
            }
        }
        return $recs;
    }

    function get_user_by_id($id)
    {
        // from the users table
        $u = $this->user->get_user($id);
        $sql = "SELECT * FROM useraux WHERE id = $id";
        $a = $this->db->query($sql)->fetch();
        if ($a == FALSE) {
            $u['specialty'] = NULL;
            $u['languages'] = NULL;
            $u['startdt'] = NULL;
            $u['enddt'] = NULL;
        }
        else {
            $u['specialty'] = $a['specialty'];
            $u['languages'] = $a['languages'];
            $u['startdt'] = $a['startdt'];
            $u['enddt'] = $a['enddt'];
        }
        return $u;
    }

    function delete($id)
    {
        $cant = FALSE;

        $sql = "SELECT * FROM issues WHERE uid = $id";
        $iss = $this->db->query($sql)->fetch();
        if (is_array($iss)) {
            $cant = TRUE;
        }

        $sql = "SELECT * FROM targets WHERE assignedto = $id OR compedby = $id";
        $tgt = $this->db->query($sql)->fetch();
        if (is_array($tgt)) {
            $cant = TRUE;
        }

        if (!$cant) {
            // user table
            $this->user->delete_user($id);
            dblog('delete', 'users');
            // useraux table
            $this->db->delete('useraux', "id = $id");
            dblog('delete', 'useraux');
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function update($post)
    {
        // check for permission to change levels
        $u = $this->user->get_user($post['id']);
        if ($this->current_user['level'] != 0) {
            if ($post['level'] != $u['level']) {
                emsg('F', "User can't change his own user level.");
            }
            $post['level'] == $u['level'];
        }

        // update users table
        // this will filter out fields which don't belong to that table
        $this->user->update_user($post);
        dblog('update', 'users');

        $prec = $this->db->prepare('useraux', $post);
        $this->db->update('useraux', $prec, "id = {$post['id']}");
        dblog('update', 'useraux');
        emsg('S', "User {$post['name']} updated.");
        return TRUE;
    }
}

