<?php

class txnlog
{
    function __construct()
    {
        global $db;
        $this->db = $db;
    }

    function fetch()
    {
        $sql = "SELECT log.*, useraux.login AS login FROM log JOIN useraux ON (useraux.id = log.uid)";
        $results = $this->db->query($sql)->fetch_all();
        $max = is_array($results) ? count($results) : 0;
        for ($i = 0; $i < $max; $i++) {
            $results[$i]['x_timestamp'] = date('Y-m-d H:i:s', $results[$i]['timestamp']);
        }
        return $results;
    }

    function clear()
    {
        $this->db->delete('log');
    }

}
