<?php include VIEWDIR . 'head.view.php'; ?>
<?php extract($data); ?>
<?php if ($events == FALSE): ?>
<h2>No log records</h2>
<?php else: ?>
<table>
<tr><th>Timestamp</th><th>User</th><th>Event</th></tr>
<?php foreach ($events as $event): ?>
<tr>
<td><?php echo $event['x_timestamp']; ?></td>
<td><?php echo $event['login']; ?></td>
<td><?php echo $event['event']; ?></td>
</tr>
<?php endforeach; ?>
</table>
<?php endif; ?>
<?php include VIEWDIR . 'foot.view.php'; ?>
