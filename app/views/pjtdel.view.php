<?php include VIEWDIR . 'head.view.php'; ?>
<?php extract($data); ?>
<form method="post" action="<?php echo $this->return; ?>">
<?php $this->form->hidden('id'); ?>
<table>
<tr>
<td><label>Name</label></td>
<td><?php echo $project['name']; ?></td>
</tr>
<tr>
<td><label>Description</label></td>
<td><?php echo $project['descrip']; ?></td>
</tr>
<tr>
<td><label>Language(s)</label></td>
<td><?php echo $project['language']; ?></td>
</tr>
</table>
<span class="red">Deletion will remove all history. Are you SURE?</span>&nbsp;<?php $this->form->submit('delete'); ?>
</form>
<?php include VIEWDIR . 'foot.view.php'; ?>
