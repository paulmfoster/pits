<?php include VIEWDIR . 'head.view.php'; ?>
<?php extract($data); ?>

<table>
<tr><th colspan="6">Deployment Header</th></tr>
<tr><td class="tdlabel">Project Name</td><td><?php echo $h['project_name']; ?></td></tr>
<tr><td class="tdlabel">Project Description</td><td><?php echo $h['project_descrip']; ?></td></tr>
<tr><td class="tdlabel">Template Description</td><td><?php echo $h['descrip']; ?></td></tr>
<tr><td class="tdlabel">Start Date</td><td><?php echo date('Y-m-d H:i:s', $h['startdate']); ?></td></tr>
<tr><td class="tdlabel">Completion Date</td><td><?php echo (is_null($h['compdate'])) ? '' : date('Y-m-d H:i:s', $h['compdate']); ?></td></tr>
<tr><td class="tdlabel">Status</td><td><?php echo $h['x_status']; ?></td></tr>
</table>
&nbsp;
<table>
<tr><th>Order</th><th>Text</th><th>Completion Date</th><th>Completed By</th></tr>
<?php foreach ($d as $dx): ?>
<tr>
<td><?php echo $dx['tgtord']; ?></td>
<td><?php echo $dx['tgttext']; ?></td>
<td><?php echo (is_null($dx['compdate'])) ? '' : date('Y-m-d H:i:s', $dx['compdate']); ?></td>
<td><?php echo $dx['login']; ?></td>
</tr>
<?php endforeach; ?>
</table>
<br/>
<?php form::button('Edit Deployment', 'index.php?url=rf/edit/' . $h['id']); ?>
&nbsp;
<?php form::button('Delete Deployment', 'index.php?url=rf/delete/' . $h['id']); ?>

<?php include VIEWDIR . 'foot.view.php'; ?>
