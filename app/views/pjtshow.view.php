<?php include VIEWDIR . 'head.view.php'; ?>
<?php extract($data); ?>
<?php global $sev_slugs, $pri_slugs, $status_slugs; ?>

<span style="font-size: 150%; font-weight: bold">Project</span>
&nbsp;
<?php form::button('Edit', 'index.php?url=pjt/edit/' . $pjt['id']); ?>
<?php form::button('Delete', 'index.php?url=pjt/delete/' . $pjt['id']); ?>
<?php form::button('Show History', 'index.php?url=pjt/hshow/' . $pjt['id']); ?>
<?php form::button('No History', 'index.php?url=pjt/show/' . $pjt['id']); ?>
<hr/>
<table>
    <tr>
        <td><label>ID</label></td>
        <td><?php echo $pjt['id']; ?></td>
        <td><label>Name</label></td>
        <td><?php echo $pjt['name']; ?></td>
        <td><label>Description</label></td>
        <td><?php echo $pjt['descrip']; ?></td>
    </tr>
    <tr>
        <td><label>Reponsible</label></td>
        <td><?php echo $pjt['login']; ?></td>
        <td><label>Language</label></td>
        <td><?php echo $pjt['language']; ?></td>
        <td colspan="2"></td>
    </tr>
    <tr>
        <td><label>Notes</label></td>
        <td colspan="4"><?php echo $pjt['notes']; ?></td>
    </tr>
</table>
<hr/>
<p></p>
<span style="font-size: 150%; font-weight: bold">Issues</span>&nbsp;<?php form::button('Add', 'index.php?url=iss/add/' . $pjt['id']); ?>

<hr/>

<?php if ($isses == FALSE): ?>
<h2>No issues</h2>
<?php else: ?>

<?php foreach ($isses as $iss): ?>
<?php form::button('Edit', 'index.php?url=iss/edit/' . $iss['id']); ?>
<?php form::button('Delete', 'index.php?url=iss/delete/' . $iss['id']); ?>
<?php form::button('Complete', 'index.php?url=iss/complete/' . $iss['id']); ?>
<p></p>
<table>
    <tr>
        <td><label>ID</label></td><td><?php echo $iss['id']; ?></td>
        <td><label>Version</label></td><td><?php echo $iss['version']; ?></td>
        <td><label>Platform</label></td><td><?php echo $iss['platform']; ?></td>
    </tr>
    <tr>
        <td><label>Severity</label></td><td><?php echo $sev_slugs[$iss['severity']]; ?></td>
        <td><label>Reported By</label></td><td><?php echo $iss['login']; ?></td>
        <td><label>Description</label></td><td><?php echo $iss['descrip']; ?></td>
    </tr>
    <tr>
        <td><label>Priority</label></td><td><?php echo $pri_slugs[$iss['priority']]; ?></td>
        <td><label>Report Date</label></td><td><?php echo date('Y-m-d H:i:s', $iss['reportdate']); ?></td>
    </tr>
    <tr>
        <td><label>Status</label></td><td><?php echo $status_slugs[$iss['status']]; ?></td>
        <td><label>Comp Date</label></td><td><?php echo (!is_null($iss['compdate'])) ? date('Y-m-d H:i:s', $iss['reportdate']) : ''; ?></td>
        <td colspan="2"></td>
    </tr>
</table>
<hr/>
<?php endforeach; ?>

<?php endif; ?>

<p></p>
    <span style="font-size: 150%; font-weight: bold">Deployments</span>&nbsp;<?php form::button('Add', 'index.php?url=rf/add/' . $pjt['id']); ?>
<hr/>

<?php if ($progs == FALSE): ?>
<h2>No Deployments</h2>
<?php else: ?>

<?php foreach ($progs as $prog): ?>

<?php form::button('Edit', 'index.php?url=rf/edit/' . $prog['id']); ?>
<?php form::button('Delete', 'index.php?url=rf/delete/' . $prog['id']); ?>
<?php form::button('Complete', 'index.php?url=rf/complete/' . $prog['id']); ?>

<p></p>

<table>
    <tr>
        <td><label>ID</label></td><td><?php echo $prog['id']; ?></td>
        <td><label>Description</label></td><td><?php echo $prog['descrip']; ?></td>
        <td><label>Start Date</label></td><td><?php echo date('Y-m-d H:i:s', $prog['startdate']); ?></td>
    </tr>
    <tr>
        <td><label>Status</label></td><td><?php echo $prog['x_status']; ?></td>
        <td colspan="2"></td>
        <td><label>Compl Date</label></td><td><?php echo (is_null($prog['compdate'])) ? '' : date('Y-m-d H:i:s', $prog['compdate']); ?></td>
    </tr>
</table>

<hr/>

<p></p>
<table>
<?php $row = 1; ?>
<?php foreach ($prog['detail'] as $dtl): ?>
    <tr class="row<?php echo $row++ & 1; ?>">
        <td><label>ID</label></td><td><?php echo $dtl['id']; ?></td>
        <td><label>Order</label></td><td><?php echo $dtl['tgtord']; ?></td>
        <td><label>Text</label></td><td><?php echo $dtl['tgttext']; ?></td>
        <td><label>Comp Date</label></td><td><?php echo (is_null($dtl['compdate'])) ? '' : date('Y-m-d H:i:s', $dtl['compdate']); ?></td>
        <td><label>Login</label></td><td><?php echo $dtl['login']; ?></td>
    </tr>
<?php endforeach; ?>
</table>
<hr/>
<?php endforeach; ?>
<?php endif; ?>

<?php include VIEWDIR . 'foot.view.php'; ?>
