<?php include VIEWDIR . 'head.view.php'; ?>
<?php extract($data); ?>
<?php $row = 0; ?>
<table>
<tr><th></th><th>Level</th><th>Login</th><th>Name</th><th>Started</th><th>Terminated</th><th>Specialty/<br/>Languages</th></tr>
<?php foreach ($users as $user): ?>
<tr class="row<?php echo $row++ & 1; ?>">
<td>
<a href="index.php?url=usr/edit/<?php echo $user['id']; ?>">Edit</a>
&nbsp;
<a href="index.php?url=usr/delete/<?php echo $user['id']; ?>">Delete</a>
<td>
<?php
switch ($user['level']) {
case 0:
    echo 'Administrator';
    break;
case PGMR_LEVEL:
    echo 'Programmer';
    break;
default:
    echo 'User';
    break;
}
?>
</td>
<td><?php echo $user['login']; ?></td>
<td><?php echo $user['name']; ?></td>
<td><?php echo (isset($user['startdt'])) ? pdate::iso2am($user['startdt']) : '&nbsp;'; ?></td>
<td><?php echo (isset($user['enddt'])) ? pdate::iso2am($user['enddt']) : '&nbsp;'; ?></td>
<td><?php echo $user['specialty']; ?><br/>
<?php echo $user['languages']; ?></td>
<?php endforeach; ?>
</table>
<?php include VIEWDIR . 'foot.view.php'; ?>
