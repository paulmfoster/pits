<?php include VIEWDIR . 'head.view.php'; ?>
<?php global $sev_slugs, $pri_slugs, $status_slugs; ?>
<?php extract($data); ?>
<?php if ($isses == FALSE): ?>
<h2>There are no issues for this project.</h2>
<?php form::button('Add Issue', 'index.php?url=iss/add/' . $pid); ?>
<?php else: ?>
<?php $row = 0; ?>
<h2>Issues for project: <a href="index.php?url=pjt/show/<?php echo $isses[0]['pid']; ?>"><?php echo $isses[0]['project_name']; ?></a></h2>
Click on issue ID to view a particular issue.
<table>
<tr>
<th>ID</th>
<th>Date</th>
<th>Version<br/>Platform</th>
<th>Sev</th>
<th>Pri</th>
<th>Status</th>
<th>Description</th>
<th>Reported</th>
<th>Completed</th>
</tr>
<?php foreach ($isses as $iss): ?>
<tr class="row<?php echo $row++ & 1; ?>">
<td><a href="index.php?url=iss/show/<?php echo $iss['id']; ?>"><?php echo $iss['id']; ?></a></td>
<td><?php echo date('Y-m-d H:i:s', $iss['reportdate']); ?></td>
<td><?php echo $iss['version'] . '<br/>' . $iss['platform']; ?></td>
<td><?php echo $sev_slugs[$iss['severity']]; ?></td>
<td><?php echo $pri_slugs[$iss['priority']]; ?></td>
<td><?php echo $status_slugs[$iss['status']]; ?></td>
<td><?php echo $iss['descrip']; ?></td>
<td><?php echo $iss['login']; ?></td>
<td><?php echo (!is_null($iss['compdate'])) ? date('Y-m-d H:m:s', $iss['compdate']) : ''; ?></td>
</tr>
<?php endforeach; ?>
</table>
<?php form::button('Add Issue', 'index.php?url=iss/add'); ?>
<?php endif; ?>
<?php include VIEWDIR . 'foot.view.php'; ?>
