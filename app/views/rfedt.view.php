<?php include VIEWDIR . 'head.view.php'; ?>
<?php extract($data); ?>
<form method="post" action="<?php echo $this->return; ?>">
<?php $this->form->hidden('id'); ?>
<table>
<tr><th colspan="6">Deployment Header</th></tr>
<tr><td class="tdlabel">Project Name</td><td><?php echo $h['project_name']; ?></td></tr>
<tr><td class="tdlabel">Project Description</td><td><?php echo $h['project_descrip']; ?></td></tr>
<tr><td class="tdlabel">Template Description</td><td><?php echo $h['descrip']; ?></td></tr>
<tr><td class="tdlabel">Start Date</td><td><?php echo date('Y-m-d H:i:s', $h['startdate']); ?></td></tr>
<tr><td class="tdlabel">Completion Date</td><td><?php echo (is_null($h['compdate'])) ? '' : date('Y-m-d H:i:s', $h['compdate']); ?></td></tr>
<tr><td class="tdlabel">Status</td><td><?php echo $h['x_status']; ?></td></tr>
</table>
&nbsp;
<table>
<tr><th>Order</th><th>Text</th><th>Completed</th></tr>
<?php foreach ($d as $dx): ?>
<tr>
<td><?php $this->form->text('tgtord_' . $dx['id']); ?></td>
<td><?php $this->form->text('tgttext_' . $dx['id']); ?></td>
<td><?php $this->form->checkbox('comped_' . $dx['id']); ?></td>
</tr>
<?php endforeach; ?>
</table>
<br/>
<?php $this->form->submit('save'); ?>
</form>
<?php include VIEWDIR . 'foot.view.php'; ?>
