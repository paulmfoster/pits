<?php include VIEWDIR . 'head.view.php'; ?>
<?php global $sev_slugs, $pri_slugs, $status_slugs; ?>
<?php extract($data); ?>
<form method="post" action="<?php echo $this->return; ?>">
<?php $this->form->hidden('id'); ?>
<table>
    <tr><td class="tdlabel">ID</td><td><?php echo $iss['id']; ?></td></tr>
    <tr><td class="tdlabel">Project</td><td><?php echo $iss['project_name']; ?></td></tr>
    <tr><td class="tdlabel">Version</td><td><?php echo $iss['version']; ?></td></tr>
    <tr><td class="tdlabel">Platform</td><td><?php echo $iss['platform']; ?></td></tr>
    <tr><td class="tdlabel">Description</td><td><?php echo $iss['descrip']; ?></td></tr>
    <tr><td class="tdlabel">Severity</td><td><?php echo $sev_slugs[$iss['severity']]; ?></td></tr>
    <tr><td class="tdlabel">Priority</td><td><?php echo $pri_slugs[$iss['priority']]; ?></td></tr>
    <tr><td class="tdlabel">Status</td><td><?php echo $status_slugs[$iss['status']]; ?></td></tr>
    <tr><td class="tdlabel">Reported By</td><td><?php echo $iss['login']; ?></td></tr>
    <tr><td class="tdlabel">Report Date</td><td><?php echo date('Y-m-d H:i:s', $iss['reportdate']); ?></td></tr>
    <tr><td class="tdlabel">Completion Date</td><td><?php echo (!is_null($iss['compdate'])) ? date('Y-m-d H:i:s', $iss['reportdate']) : ''; ?></td></tr>
    <tr><td><span class="red">Are you <bold>SURE</bold> you want to delete this issue?</span></td>
    <td><?php $this->form->submit('delete'); ?></td>
</tr>
</table>
</form>
<?php include VIEWDIR . 'foot.view.php'; ?>
