<?php include VIEWDIR . 'head.view.php'; ?>
<?php extract($data); ?>
<?php if ($recs == FALSE): ?>
<h2>No dev cycle records</h2>
<?php else: ?>
<?php global $status_slugs; ?>
<h2>Click on the deployment to view it.</h2>
<?php $row = 0; ?>
<label>Project: </label><?php echo $recs[0]['project_name'] . ' / ' . $recs[0]['project_descrip']; ?>
<table>
<tr><th>ID</th><th>Description</th><th>Start</th><th>End</th><th>Status</th></tr>
<?php foreach ($recs as $rec): ?>
<tr class="row<?php echo $row++ & 1; ?>">
<td><a href="index.php?url=rf/show/<?php echo $rec['id']; ?>"><?php echo $rec['id']; ?></a></td>
<td><?php echo $rec['descrip']; ?></td>
<td><?php echo date('Y-m-d H:i:s', $rec['startdate']); ?></td>
<td><?php echo (is_null($rec['compdate'])) ? '' : date('Y-m-d H:i:s', $rec['compdate']); ?></td>
<td><?php echo $rec['x_status']; ?></td>
</tr>
<?php endforeach; ?>
</table>
<?php endif; ?>
<?php form::button('Add Deployment', 'index.php?url=pb/add/' . $pid); ?>
<?php include VIEWDIR . 'foot.view.php'; ?>
