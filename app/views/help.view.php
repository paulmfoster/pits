<?php include VIEWDIR . 'head.view.php'; ?>
<h2>Why PITS?</h2>
As a developer, I've searched for years for tools to help me manage my development tasks.
<strong>Git</strong> was a vast improvement over other version control systems. But I
never found bug tracking software I was satisfied with. Most of them were far more complex
than I needed. Most were written in languages I didn't want to have to maintain. For example,
<strong>Trac</strong> is written in Python, and I'm not fond of Python. If I wanted to hack
Trac, I'd have to dig into Python, which I'd rather not do. <strong>MantisBT</strong> is
written in PHP, a language I know well. But it's far more complex than needed.
<p>
But there's another component of my development that I must manage: deployment. I have some
projects I only run on my local network. I have others which are hosted on Gitlab. Still others
are hosted on Internet sites. When I'm done with a series of code modifications, I have to
determine what steps are now necessary to export that code to its final destination(s).
</p>
<p>
So what I really need isn't just a bug tracker. I also need a package which will help me 
remember what I need to do with individual packages, once I'm done modifying the code.
That's why I wrote <strong>Project &amp; Issue Tracking System (PITS)</strong>.
</p>

<h2>Projects</h2>
The most important aspect of development is the <strong>project</strong>. This is the
codebase you're working on, whether it's the Linux kernel, or your personal calculator
software. PITS allows you to add, edit and delete projects. For each project, you will
fill out various fields which should be self-explanatory.

<h2>Issues</h2>
These are the bug reports and feature requests. These result in your code modifications.
In PITS, like any other bug tracker, you create these "issues", and annotate them with
descriptions and details. As you complete them, you mark them completed. PITS shows you
all the active bugs/feature requests, just like any other bug tracking system.

<h2>Templates</h2>
When it comes to deploying your code, there may be many different workflows, depending
on the project. Each of these contains a variety of steps. For a couple of your projects,
you need to push your code to your live site. And then push it to Gitlab. For other
projects, you only need to push the code to each of the two servers on your local LAN.
<p>
Each set of steps can be called a "template". You create these templates with their
various steps, and then you apply them to the project you're working on.
</p>

<h2>Deployments</h2>
These are the implementations of your "templates". In practice, you'll accumulate a number
of bugs or feature requests, and eventually package these together as a new release.
When the release is ready, you will copy a "template" of deployment steps which will
become your actual deployments for this release of this project.

<h2>Logs</h2>
A couple of the menu choices allow you to see and clear activity taking place on the 
database for PITS. You may not have much need for this function. But it does allow you to
see what kind of changes are being made to the tables which comprise the PITS records,
who's doing them, and when.

<h2>Users</h2>
Out of the box, PITS is configured for three levels of users. First are administrators.
Only administrators may create (or delete) projects. The administrator is also the only
person on the system who may create other users.
<p>
Programmers are the second level of users. Only programmers (or administrators) may
add, edit or delete issues. Only programmers and above may create templates or issues.
</p>
<p>
Last are regular users. They have no special powers, and may view most things in the
system. But they may not do anything which would change the PITS database.
</p>

<h2>Default Administrator</h2>
Out of the box, the system provides a single system administrator user called "admin".
The password for this user is "password". Your first action should be to change this
password.

<h2>Included Software</h2>
This software is based on a framework I wrote, called "Grotworx". It is an OOP MVC
framework for web applications and is available on
<a href="https://gitlab.com/paulmfoster/grotworx">Gitlab</a>. Aside from the 
framework itself, there is a great deal of software included to make your life as a
web applications programmer easier. You're welcome to use this additional software
in your projects as well.

<h2>License</h2>
This software is released under the GNU Public License (GPL) version 2. You are free
to copy, modify and redistribute the software, so long as you provide all the source
code with the software. I'm not responsible if the software doesn't do what you think
it should, particularly if you modify it. I make no promises about what it will do.

<h2>Where To Get The Software</h2>
This software is available on <a href="https://gitlab.com/paulmfoster/pits">Gitlab</a>.


<?php include VIEWDIR . 'foot.view.php'; ?>
