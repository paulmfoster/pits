<?php include VIEWDIR . 'head.view.php'; ?>
<?php extract($data); ?>
<form method="post" action="<?php echo $this->return; ?>">
<?php $this->form->hidden('id'); ?>
<table>

<tr>
<td class="tdlabel">Issue ID</td><td><?php echo $iss['id']; ?></td>
</tr>

<tr>
<td class="tdlabel">Reported By</td><td><?php echo $iss['login']; ?></td>
</tr>

<tr>
<td class="tdlabel">Report Date</td><td><?php echo date('Y-m-d H:i:s', $iss['reportdate']); ?></td>
</tr>

<tr>
<td class="tdlabel">Project</td><td><?php echo $iss['id'] . '&nbsp;' . $iss['project_name']; ?></td>
</tr>

<tr>
<td class="tdlabel">Version</td><td><?php $this->form->text('version'); ?></td>
</tr>

<tr>
<td class="tdlabel">Platform</td><td><?php $this->form->text('platform'); ?></td>
</tr>

<tr>
<td class="tdlabel">Descriptions</td><td><?php $this->form->text('descrip'); ?></td>
</tr>

<tr>
<td class="tdlabel">Severity</td><td><?php $this->form->select('severity'); ?></td>
</tr>

<tr>
<td class="tdlabel">Priority</td><td><?php $this->form->select('priority'); ?></td>
</tr>

<tr>
<td class="tdlabel">Status</td><td><?php $this->form->select('status'); ?></td>
</tr>

<tr>
<td class="tdlabel">Notes</td><td><?php $this->form->textarea('notes'); ?></td>
</tr>

<tr>
<td></td><td><?php $this->form->submit('save'); ?></td>
</tr>

</table>

</form>
<?php include VIEWDIR . 'foot.view.php'; ?>
