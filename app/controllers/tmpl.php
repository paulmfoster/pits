<?php

class tmpl extends controller
{
    function __construct()
    {
        global $nav, $form, $cfg, $current_user;

        $this->cfg = $cfg;
        $this->nav = $nav;
        $this->form = $form;
        $this->current_user = $current_user;
        $this->templates = model('templates');
    }

    function add()
    {
        access(PGMR_LEVEL, 'index.php');
        $fields = [
            'descrip' => [
                'name' => 'descrip',
                'type' => 'text',
                'size' => 30,
                'maxlength' => 30,
                'required' => 1,
                'label' => 'Description'
            ],
            'targets' => [
                'name' => 'targets',
                'type' => 'textarea',
                'rows' => 10,
                'cols' => 50,
                'required' => 1,
                'label' => 'Targets'
            ],
            'save' => [
                'name' => 'save',
                'type' => 'submit',
                'value' => 'Save'
            ]
        ];
        $this->form->set($fields);
        $this->page_title = 'Add Dev Cycle';
        $this->return = 'index.php?url=tmpl/add2';
        $this->focus_field = 'descrip';
        $this->view('tmpladd.view.php');
    }

    function add2()
    {
        $descrip = $_POST['descrip'] ?? NULL;
        if (is_null($descrip)) {
            $this->add();
        }

        $id = $this->templates->add($_POST);
        $this->show($id);
    }

    function show($id = NULL)
    {
        access(PGMR_LEVEL, 'index.php');
        if (is_null($id)) {
            $this->list();
        }
        $recs = $this->templates->get($id);
        $this->page_title = 'Show Template';
        $this->view('tmplshow.view.php', ['recs' => $recs]);
    }

    function list()
    {
        access(PGMR_LEVEL, 'index.php');
        $recs = $this->templates->list();
        $this->page_title = 'List Templates';
        $this->view('tmpllist.view.php', ['recs' => $recs]);
    }

    function edit($id = NULL)
    {
        if (is_null($id)) {
            redirect('index.php');
        }
        access(PGMR_LEVEL, 'index.php');
        $t = $this->templates->get($id);
        $fields = [
            'id' => [
                'name' => 'id',
                'type' => 'hidden',
                'value' => $id
            ],
            'descrip' => [
                'name' => 'descrip',
                'type' => 'text',
                'value' => $t[0]['descrip'],
                'size' => 30,
                'maxlength' => 30
            ],
            'save' => [
                'name' => 'save',
                'type' => 'submit',
                'value' => 'Save'
            ]
        ];

        $max = count($t);
        $ids = [];
        for ($i = 0; $i < $max; $i++) {
            $fields['tgtord_' . $t[$i]['id']] = [
                'name' => 'tgtord_' . $t[$i]['id'],
                'type' => 'text',
                'size' => 6,
                'maxlength' => 6,
                'value' => $t[$i]['tgtord']
            ];
            $fields['tgttext_' . $t[$i]['id']] = [
                'name' => 'tgttext_' . $t[$i]['id'],
                'type' => 'text',
                'size' => 50,
                'maxlength' => 50,
                'value' => $t[$i]['tgttext']
            ];
            $ids[] = $t[$i]['id'];
        }

        $this->form->set($fields);
        $this->page_title = 'Edit Template';
        $this->focus_field = 'descrip';
        $this->return = 'index.php?url=tmpl/edit2';
        $this->view('tmpledt.view.php', ['recs' => $t, 'max' => $max, 'ids' => $ids]);
    }

    function edit2()
    {
        if (is_null($_POST['id'])) {
            $this->list();
        }

        $this->templates->update($_POST);
        emsg('S', 'Template edits saved.');
        $this->show($_POST['id']);
    }

    function delete($id = NULL)
    {
        if (is_null($id)) {
            $this->list();
        }

        $result = $this->templates->delete($id);
        if ($result) {
            emsg('S', 'Deployment template deleted.');
        }
        else {
            emsg('F', "Template in use; can't delete.");
        }
        $this->list();
    }
}
