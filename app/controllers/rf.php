<?php

class rf extends controller
{
    function __construct()
    {
        global $nav, $form, $cfg, $current_user;

        $this->cfg = $cfg;
        $this->nav = $nav;
        $this->form = $form;
        $this->current_user = $current_user;
        $this->rforms = model('rforms');
        $this->templates = model('templates');
    }

    function list($pid = NULL)
    {
        if (is_null($pid)) {
            redirect('index.php');
        }

        $recs = $this->rforms->list($pid);
        $this->page_title = 'List Deployments';
        $this->view('rflist.view.php', ['recs' => $recs, 'pid' => $pid]);
    }

    function add($pid = NULL) 
    {
        access(PGMR_LEVEL, 'index.php');
        $tmpls = $this->templates->list();
        if ($tmpls == FALSE) {
            emsg('F', 'Before adding a deployment, you must add a template.');
            redirect('index.php?url=tmpl/add');
        }

        if (is_null($pid)) {
            redirect('index.php');
        }

        $tmpl_options = [];
        foreach ($tmpls as $tmpl) {
            $tmpl_options[] = ['lbl' => $tmpl['descrip'], 'val' => $tmpl['id']];
        }

        $fields = [
            'pid' => [
                'name' => 'pid',
                'type' => 'hidden',
                'value' => $pid
            ],
            'id' => [
                'name' => 'id',
                'type' => 'select',
                'options' => $tmpl_options,
                'label' => 'Templates'
            ],
            'select' => [
                'name' => 'select',
                'type' => 'submit',
                'value' => 'Select'
            ]
        ];
        $this->form->set($fields);
        $this->page_title = 'Add Deployment';
        $this->return = 'index.php?url=rf/add2';
        $this->focus_field = 'id';
        $this->view('rfadd.view.php');
    }

    function add2()
    {
        $id = $_POST['id'] ?? NULL;
        $pid = $_POST['pid'] ?? NULL;
        if (is_null($id) || is_null($pid)) {
            redirect('index.php');
        }
        $lastid = $this->rforms->add($_POST);

        $this->list($_POST['pid']);
    }

    function show($id = NULL)
    {
        if (is_null($id)) {
            $this->list();
        }
        $dc = $this->rforms->get($id);

        $this->page_title = 'Show Deployment';
        $this->view('rfshow.view.php', ['h' => $dc['head'], 'd' => $dc['detail']]);
    }

    function delete($id = NULL)
    {
        if (is_null($id)) {
            redirect('index.php');
        }
        access(PGMR_LEVEL, 'index.php');
        $this->rforms->delete($id);
        emsg('S', 'Deployment deleted.');
        show_project();
    }

    function edit($id = NULL)
    {
        if (is_null($id)) {
            $this->list();
        }
        access(PGMR_LEVEL, 'index.php');
        $dc = $this->rforms->get($id);

        foreach ($dc['detail'] as $d) {
            $fields['tgtord_' . $d['id']] = [
                'name' => 'tgtord_' . $d['id'],
                'type' => 'text',
                'size' => 6,
                'maxlength' => 6,
                'value' => $d['tgtord']
            ];
            $fields['tgttext_' . $d['id']] = [
                'name' => 'tgttext_' . $d['id'],
                'type' => 'text',
                'size' => 50,
                'maxlength' => 50,
                'value' => $d['tgttext']
            ];
            $fields['comped_' . $d['id']] = [
                'name' => 'comped_' . $d['id'],
                'type' => 'checkbox',
                'value' => 1
            ];
        }
        $fields['id'] = [
            'name' => 'id',
            'type' => 'hidden',
            'value' => $id
        ];
        $fields['save'] = [
            'name' => 'saved',
            'type' => 'submit',
            'value' => 'Save Edits'
        ];
        $this->form->set($fields);

        $this->page_title = 'Edit Deployment';
        $this->return = 'index.php?url=rf/edit2';
        $this->view('rfedt.view.php', ['h' => $dc['head'], 'd' => $dc['detail']]);
    }

    function edit2()
    {
        $id = $_POST['id'] ?? NULL;
        if (is_null($id)) {
            redirect('index.php');
        }
        $this->rforms->update($_POST);
        $this->show($_POST['id']);
    }

    function complete($rformid = NULL)
    {
        if (is_null($rformid)) {
            redirect('index.php');
        }
        access(PGMR_LEVEL, 'index.php?url=rf/show/' . $rformid);
        $pid = $this->rforms->complete($rformid);
        show_project();
    }
}
