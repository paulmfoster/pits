<?php

class usr extends controller
{
    function __construct()
    {
        global $nav, $form, $cfg, $db, $user, $current_user;

        $this->db = $db;
        $this->cfg = $cfg;
        $this->nav = $nav;
        $this->form = $form;
        $this->user = $user;
        $this->users = model('users');
        $this->current_user = $current_user;
    }

    function list()
    {
        $recs = $this->users->list();    

        $this->page_title = 'User List';
        $this->view('userlist.view.php', ['users' => $recs]);
    }

    function edit($id = NULL)
    {
        if (is_null($id)) {
            $this->list();
        }
        access(PGMR_LEVEL, 'index.php?url=/usr/list');
        $u = $this->users->get_user_by_id($id);

        if ($this->current_user['id'] != $id && $this->current_user['level'] != 0) {
            emsg('F', "You are not allowed to edit this user.");
            $this->list();
        }

        $level_options = [
            ['lbl' => 'Administrator', 'val' => 0],
            ['lbl' => 'Programmer', 'val' => PGMR_LEVEL],
            ['lbl' => 'User', 'val' => 255]
        ];
        $fields = [
            'id' => [
                'name' => 'id',
                'type' => 'hidden',
                'value' => $id
            ],
            'login' => [
                'name' => 'login',
                'type' => 'text',
                'size' => 30,
                'maxlength' => 30,
                'value' => $u['login'],
                'label' => 'Login'
            ],
            'name' => [
                'name' => 'name',
                'type' => 'text',
                'size' => 50,
                'maxlength' => 50,
                'value' => $u['name'],
                'label' => 'Name'
            ],
            'email' => [
                'name' => 'email',
                'type' => 'text',
                'size' => 50,
                'maxlength' => 255,
                'value' => $u['email'],
                'label' => 'Email Address'
            ],
            'password' => [
                'name' => 'password',
                'type' => 'password',
                'size' => 20,
                'maxlength' => 255,
                'label' => 'Password'
            ],
            'confirm' => [
                'name' => 'confirm',
                'type' => 'password',
                'size' => 20,
                'maxlength' => 255,
                'label' => 'Confirm Password'
            ],
            'level' => [
                'name' => 'level',
                'type' => 'select',
                'options' => $level_options,
                'label' => 'Level',
                'value' => $u['level']
            ],
            'specialty' => [
                'name' => 'specialty',
                'type' => 'text',
                'size' => 50,
                'maxlength' => 50,
                'value' => $u['specialty'],
                'label' => 'Specialty'
            ],
            'languages' => [
                'name' => 'languages',
                'type' => 'text',
                'size' => 50,
                'maxlength' => 50,
                'value' => $u['languages'],
                'label' => 'Languages'
            ],
            'enddt' => [
                'name' => 'enddt',
                'type' => 'date',
                'label' => 'Termination Date'
            ],
            'save' => [
                'name' => 'save',
                'type' => 'submit',
                'value' => 'Save'
            ]
        ];
        $this->form->set($fields);
        $this->page_title = 'Edit User';
        $this->return = 'index.php?url=usr/edit2';
        $this->view('useredt.view.php');
    }

    function edit2()
    {
        $id = $_POST['id'] ?? NULL;
        if (is_null($id)) {
            $this->edit();
        }
        $this->users->update($_POST);
        $this->list();
    }

    function delete($id = NULL)
    {
        if (is_null($id)) {
            $this->list();
        }
        access(PGMR_LEVEL, 'index.php?url=/usr/list');
        $u = $this->users->get_user_by_id($id);

        $fields = [
            'id' => [
                'name' => 'id',
                'type' => 'hidden',
                'value' => $id
            ],
            'delete' => [
                'name' => 'delete',
                'type' => 'submit',
                'value' => 'Delete'
            ]
        ];

        $this->form->set($fields);
        $this->page_title = 'Delete User';
        $this->return = 'index.php?url=usr/delete2';
        $this->view('userdel.view.php', ['auser' => $u]);
    }

    function delete2()
    {
        $id = $_POST['id'] ?? NULL;
        if (!is_null($id)) {
            $okay = $this->users->delete($id);
            if ($okay) {
                emsg('S', 'User deleted.');
            }
            else {
                emsg('F', 'User owns issues/targets and cannot be deleted.');
            }
        }
        $this->list();
    }

    function login()
    {
        $fields = array(
            'login' => array(
                'name' => 'login',
                'type' => 'text',
                'size' => 12,
                'maxlength' => 30,
                'required' => 1,
                'class' => 'required'
            ),
            'password' => array(
                'name' => 'password',
                'type' => 'password',
                'required' => 1,
                'class' => 'required',
                'size' => 20,
                'maxlength' => 255
            ),
            's1' => array(
                'name' => 's1',
                'type' => 'submit',
                'value' => 'Login'
            )
        );

        $this->form->set($fields);
        $this->page_title = 'Log In';
        $this->focus_field = 'login';
        $this->return = 'index.php?url=usr/login2';
        $this->view('login.view.php');
    }

    function login2()
    {
        $login = $_POST['login'] ?? NULL;
        if (!is_null($login)) {
            if ($this->users->login($_POST)) {
                emsg('S', 'You are now logged in');
            }
            else {
                emsg('F', 'Login FAILED');
            }
        }

        redirect('index.php');
    }

    function logout()
    {
        unset($_SESSION['user']);
        emsg('S', 'You are logged out.');
        redirect('index.php');
    }

    function add()
    {
        access(0, 'index.php');
        $level_options = [
            ['lbl' => 'Administrator', 'val' => 0],
            ['lbl' => 'Programmer', 'val' => PGMR_LEVEL],
            ['lbl' => 'User', 'val' => 255]
        ];

        $fields = [
            'login' => [
                'name' => 'login',
                'type' => 'text',
                'size' => 30,
                'maxlength' => 30,
                'required' => 1,
                'label' => 'Login'
            ],
            'name' => [
                'name' => 'name',
                'type' => 'text',
                'size' => 50,
                'maxlength' => 50,
                'required' => 1,
                'label' => 'User Name'
            ],
            'email' => [
                'name' => 'email',
                'type' => 'text',
                'size' => 50,
                'maxlength' => 255,
                'required' => 1,
                'label' => 'Email Address'
            ],
            'password' => [
                'name' => 'password',
                'type' => 'password',
                'size' => 20,
                'maxlength' => 255,
                'required' => 1,
                'label' => 'Password'
            ],
            'confirm' => [
                'name' => 'password',
                'type' => 'password',
                'size' => 20,
                'maxlength' => 255,
                'required' => 1,
                'label' => 'Confirm Password'
            ],
            'level' => [
                'name' => 'level',
                'type' => 'select',
                'options' => $level_options,
                'label' => 'Level'
            ],
            'specialty' => [
                'name' => 'specialty',
                'type' => 'text',
                'size' => 50,
                'maxlength' => 50,
                'label' => 'Specialty'
            ],
            'languages' => [
                'name' => 'languages',
                'type' => 'text',
                'size' => 50,
                'maxlength' => 50,
                'label' => 'Languages'
            ],
            'save' => [
                'name' => 'save',
                'type' => 'submit',
                'value' => 'Save'
            ]
        ];
        $this->form->set($fields);
        $this->focus_field = 'login';
        $this->page_title = 'Add User';
        $this->return = 'index.php?url=usr/add2';
        $this->view('useradd.view.php');

    }

    function add2()
    {
        if (!isset($_POST)) {
            $this->add();
        }

        $this->users->add($_POST);
        $this->add();
    }

    
}
