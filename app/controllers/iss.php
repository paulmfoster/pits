<?php

class iss extends controller
{
    function __construct()
    {
        global $nav, $form, $cfg, $current_user;

        $this->cfg = $cfg;
        $this->nav = $nav;
        $this->form = $form;
        $this->current_user = $current_user;
        $this->issues = model('issues');
        $this->projects = model('projects');
    }

    // pid selection is part of this screen
    function add($pid = NULL)
    {
        global $sev_slugs, $pri_slugs;
        if (is_null($pid)) {
            redirect('index.php');
        }

        access(PGMR_LEVEL, 'index.php');

        $proj = $this->projects->brief($pid);

        $sev_options = [
            ['lbl' => $sev_slugs[SEV_BLOCK], 'val' => SEV_BLOCK],
            ['lbl' => $sev_slugs[SEV_CRASH], 'val' => SEV_CRASH],
            ['lbl' => $sev_slugs[SEV_FAIL], 'val' => SEV_FAIL],
            ['lbl' => $sev_slugs[SEV_DESIGN], 'val' => SEV_DESIGN],
            ['lbl' => $sev_slugs[SEV_FEATURE], 'val' => SEV_FEATURE]
        ];

        $pri_options = [
            ['lbl' => $pri_slugs[PRI_URGENT], 'val' => PRI_URGENT],
            ['lbl' => $pri_slugs[PRI_HIGH], 'val' => PRI_HIGH],
            ['lbl' => $pri_slugs[PRI_MEDIUM], 'val' => PRI_MEDIUM],
            ['lbl' => $pri_slugs[PRI_LOW], 'val' => PRI_LOW]
        ];

        $fields = [
            'reportedby' => [
                'name' => 'reportedby',
                'type' => 'hidden',
                'value' => $this->current_user['id']
            ],
            'pid' => [
                'name' => 'pid',
                'type' => 'hidden',
                'value' => $pid
            ],
            'version' => [
                'name' => 'version',
                'type' => 'text',
                'size' => 30,
                'maxlength' => 30,
                'label' => 'Version'
            ],
            'platform' => [
                'name' => 'platform',
                'type' => 'text',
                'size' => 30,
                'maxlength' => 30,
                'label' => 'Platform'
            ],
            'descrip' => [
                'name' => 'descrip',
                'type' => 'text',
                'size' => 50,
                'maxlength' => 50,
                'label' => 'Description',
                'required' => 1
            ],
            'severity' => [
                'name' => 'severity',
                'type' => 'select',
                'options' => $sev_options,
                'required' => 1,
                'label' => 'Severity'
            ],
            'priority' => [
                'name' => 'priority',
                'type' => 'select',
                'required' => 1,
                'options' => $pri_options,
                'label' => 'Priority'
            ],
            'save' => [
                'name' => 'save',
                'type' => 'submit',
                'value' => 'Save'
            ]
        ];
        $this->form->set($fields);
        $this->page_title = 'Add Issue';
        $this->return = 'index.php?url=iss/add2';
        $this->view('issadd.view.php', ['proj' => $proj]);        
    }

    function add2()
    {
        $pid = $_POST['pid'] ?? NULL;
        if (is_null($pid)) {
            $this->add();
        }

        $_POST['status'] = STA_NEW;
        $_POST['reportdate'] = time();
        $result = $this->issues->add($_POST);
        if ($result !== FALSE) {
            emsg('S', "Issue $result added.");
        }
        else {
            emsg('F', 'Issue NOT added.');
        }
        $this->add();
    }

    function list($pid = NULL)
    {
        if (is_null($pid)) {
            redirect('index.php');
        }

        $isses = $this->issues->list($pid);

        $this->page_title = 'Issues List';
        $this->view('isslist.view.php', ['pid' => $pid, 'isses' => $isses]);
    }

    function show($id = NULL)
    {
        if (is_null($id)) {
            redirect('index.php');
        }

        $iss = $this->issues->by_id($id);
        $this->page_title = 'Show Issue';
        $this->view('issshow.view.php', ['iss' => $iss]);
    }

    function edit($id = NULL)
    {
        global $sev_slugs, $pri_slugs, $status_slugs;

        access(PGMR_LEVEL, 'index.php');

        if (is_null($id)) {
            redirect('index.php');
        }

        $rec = $this->issues->by_id($id);

        $sev_options = [
            ['lbl' => $sev_slugs[SEV_BLOCK], 'val' => SEV_BLOCK],
            ['lbl' => $sev_slugs[SEV_CRASH], 'val' => SEV_CRASH],
            ['lbl' => $sev_slugs[SEV_FAIL], 'val' => SEV_FAIL],
            ['lbl' => $sev_slugs[SEV_DESIGN], 'val' => SEV_DESIGN],
            ['lbl' => $sev_slugs[SEV_FEATURE], 'val' => SEV_FEATURE]
        ];

        $pri_options = [
            ['lbl' => $pri_slugs[PRI_URGENT], 'val' => PRI_URGENT],
            ['lbl' => $pri_slugs[PRI_HIGH], 'val' => PRI_HIGH],
            ['lbl' => $pri_slugs[PRI_MEDIUM], 'val' => PRI_MEDIUM],
            ['lbl' => $pri_slugs[PRI_LOW], 'val' => PRI_LOW]
        ];

        $status_options = [
            ['lbl' => $status_slugs[STA_NEW], 'val' => STA_NEW],
            ['lbl' => $status_slugs[STA_ACK], 'val' => STA_ACK],
            ['lbl' => $status_slugs[STA_CNF], 'val' => STA_CNF],
            ['lbl' => $status_slugs[STA_ASS], 'val' => STA_ASS],
            ['lbl' => $status_slugs[STA_RSLVD], 'val' => STA_RSLVD],
            /* ['lbl' => $status_slugs[STA_CLSD], 'val' => STA_CLSD], */
            ['lbl' => $status_slugs[STA_FXD], 'val' => STA_FXD],
            ['lbl' => $status_slugs[STA_DUP], 'val' => STA_DUP],
            ['lbl' => $status_slugs[STA_WONT], 'val' => STA_WONT],
            ['lbl' => $status_slugs[STA_NCR], 'val' => STA_NCR]
        ];

        $fields = [
            'id' => [
                'name' => 'id',
                'type' => 'hidden',
                'value' => $id
            ],
            'version' => [
                'name' => 'version',
                'type' => 'text',
                'size' => 30,
                'maxlength' => 30,
                'value' => $rec['version']
            ],
            'platform' => [
                'name' => 'platform',
                'type' => 'text',
                'size' => 30,
                'maxlength' => 30,
                'value' => $rec['platform']
            ],
            'descrip' => [
                'name' => 'descrip',
                'type' => 'text',
                'size' => 50,
                'maxlength' => 50,
                'value' => $rec['descrip'],
                'required' => 1
            ],
            'severity' => [
                'name' => 'severity',
                'type' => 'select',
                'options' => $sev_options,
                'required' => 1,
                'value' => $rec['severity']
            ],
            'priority' => [
                'name' => 'priority',
                'type' => 'select',
                'required' => 1,
                'options' => $pri_options,
                'value' => $rec['priority']
            ],
            'status' => [
                'name' => 'status',
                'type' => 'select',
                'required' => 1,
                'options' => $status_options,
                'value' => $rec['status']
            ],
            'notes' => [
                'name' => 'notes',
                'type' => 'textarea',
                'wrap' => 'soft',
                'cols' => 50,
                'rows' => 10,
                'maxlength' => 255,
                'value' => $rec['notes']
            ],
            'save' => [
                'name' => 'save',
                'type' => 'submit',
                'value' => 'Save Edits'
            ]
        ];
        $this->form->set($fields);
        $this->page_title = 'Edit Issue';
        $this->return = 'index.php?url=iss/edit2';
        $this->view('issedt.view.php', ['iss' => $rec]);
    }

    function edit2()
    {
        $id = $_POST['id'] ?? NULL;
        if (is_null($id)) {
            redirect('index.php');
        }

        $this->issues->update($_POST);
        emsg('S', 'Issue edits saved.');
        $this->show($id);
    }

    function delete($id = NULL)
    {
        if (is_null($id)) {
            redirect('index.php');
        }
        access(PGMR_LEVEL, 'index.php');

        $rec = $this->issues->by_id($id);

        $fields = [
            'id' => [
                'name' => 'id',
                'type' => 'hidden',
                'value' => $id
            ],
            'delete' => [
                'name' => 'delete',
                'type' => 'submit',
                'value' => 'Delete'
            ]
        ];
        $this->form->set($fields);

        $this->page_title = 'Delete Issue';
        $this->return = 'index.php?url=iss/delete2';
        $this->view('issdel.view.php', ['iss' => $rec]);
    }

    function delete2()
    {
        $id = $_POST['id'] ?? NULL;
        if (is_null($id)) {
            redirect('index.php');
        }

        $this->issues->delete($id);
        emsg('S', 'Issue deleted.');
        show_project();
    }

    function complete($id = NULL)
    {
        if (is_null($id)) {
            redirect('index.php');
        }
        access(PGMR_LEVEL, 'index.php');
        $this->issues->complete($id);
        emsg('S', 'Issue completed.');
        $this->show($id);
    }
}
