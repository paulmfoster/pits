<?php

class log extends controller
{
    function __construct()
    {
        global $nav, $form, $cfg, $current_user;

        $this->cfg = $cfg;
        $this->nav = $nav;
        $this->form = $form;
        $this->current_user = $current_user;
        $this->txnlog = model('txnlog');
    }
    
    function show()
    {
        access(0, 'index.php');
        $results = $this->txnlog->fetch();
        $this->page_title = 'Database Log';
        $this->view('logshow.view.php', ['events' => $results]);
    }

    function clear()
    {
        access(0, 'index.php');
        $this->txnlog->clear();
        emsg('S', 'Database log cleared.');
        $this->show();
    }
}
