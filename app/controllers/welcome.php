<?php

/**
 * Default controller
 */

class welcome extends controller
{
    function __construct()
    {
        global $nav, $form, $cfg, $current_user;

        $this->cfg = $cfg;
        $this->nav = $nav;
        $this->form = $form;
        $this->current_user = $current_user;
        $this->projects = model('projects');
    }

    function index()
    {
        $results = $this->projects->list();
        $this->page_title = 'Project List';
        $this->view('index.view.php', ['projects' => $results]);
    }

    function help()
    {
        $this->page_title = 'P.I.T.S. Help';
        $this->view('help.view.php');
    }
}
