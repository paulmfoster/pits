<?php

class pjt extends controller
{
    function __construct()
    {
        global $nav, $form, $cfg, $current_user;

        $this->cfg = $cfg;
        $this->nav = $nav;
        $this->form = $form;
        $this->current_user = $current_user;
        $this->projects = model('projects');
    }

    function list()
    {
        $results = $this->projects->list();
        $this->page_title = 'Project List';
        $this->view('index.view.php', ['projects' => $results]);
    }

    function add()
    {
        access(0, 'index.php?url=pjt/list');
        if (!class_exists('users')) {
            $users = model('users');
        }

        $uid_options = [];
        $usrs = $users->list();
        foreach ($usrs as $usr) {
            $uid_options[] = ['lbl' => $usr['name'], 'val' => $usr['id']];
        }

        $fields = [
            'name' => [
                'name' => 'name',
                'type' => 'text',
                'size' => 20,
                'maxlength' => 20,
                'required' => 1,
                'label' => 'Name'
            ],
            'descrip' => [
                'name' => 'descrip',
                'type' => 'text',
                'size' => 50,
                'maxlength' => 50,
                'required' => 1,
                'label' => 'Description'
            ],
            'language' => [
                'name' => 'language',
                'type' => 'text',
                'size' => 20,
                'maxlength' => 20,
                'label' => 'Language(s)'
            ],
            'responsible' => [
                'name' => 'responsible',
                'type' => 'select',
                'options' => $uid_options,
                'required' => 1,
                'label' => 'Responsibly Party'
            ],
            'notes' => [
                'name' => 'notes',
                'type' => 'textarea',
                'maxlength' => 512,
                'rows' => 10,
                'cols' => 50,
                'label' => 'Notes'
            ],
            'save' => [
                'name' => 'save',
                'type' => 'submit',
                'value' => 'Save'
            ]
        ];
        $this->form->set($fields);
        $this->focus_field = 'name';
        $this->page_title = 'Add Project';
        $this->return = 'index.php?url=pjt/add2';
        $this->view('pjtadd.view.php');
    }

    function add2()
    {
        $name = $_POST['name'] ?? NULL;
        if (is_null($name)) {
            $this->list();
        }

        $result = $this->projects->add($_POST);
        if ($result) {
            emsg('S', "Project {$_POST['name']} added.");
        }
        else {
            emsg('F', "Project name already in use. Aborting.");
        }
        $this->list();
    }

    function edit($id)
    {
        access(0, 'index.php?url=pjt/list');
        $result = $this->projects->brief($id);

        $fields = [
            'id' => [
                'name' => 'id',
                'type' => 'hidden',
                'value' => $result['id']
            ],
            'name' => [
                'name' => 'name',
                'type' => 'text',
                'size' => 20,
                'maxlength' => 20,
                'value' => $result['name'],
                'label' => 'Name'
            ],
            'descrip' => [
                'name' => 'descrip',
                'type' => 'text',
                'size' => 50,
                'maxlength' => 50,
                'value' => $result['descrip'],
                'label' => 'Description'
            ],
            'language' => [
                'name' => 'language',
                'type' => 'text',
                'size' => 20,
                'maxlength' => 20,
                'value' => $result['language'],
                'label' => 'Language(s)'
            ],
            'notes' => [
                'name' => 'notes',
                'type' => 'textarea',
                'maxlength' => 512,
                'rows' => 10,
                'cols' => 50,
                'value' => $result['notes'],
                'label' => 'Notes'
            ],
            'save' => [
                'name' => 'save',
                'type' => 'submit',
                'value' => 'Save'
            ]
        ];
        $this->form->set($fields);
        $this->page_title = 'Edit Project';
        $this->return = 'index.php?url=pjt/edit2';
        $this->view('pjtedt.view.php');
    }

    function edit2()
    {
        $id = $_POST['id'] ?? NULL;
        if (is_null($id)) {
            $this->list();
        }
        $result = $this->projects->update($_POST);
        if ($result != FALSE) {
            emsg('S', 'Project edits saved.');
        }
        else {
            emsg('F', 'Project edits NOT saved.');
        }
        $this->list();
    }

    function delete($id = NULL)
    {
        if (is_null($id)) {
            $this->list();
        }
        $project = $this->projects->brief($id);
        $fields = [
            'id' => [
                'name' => 'id',
                'type' => 'hidden',
                'value' => $id
            ],
            'delete' => [
                'name' => 'delete',
                'type' => 'submit',
                'value' => 'Delete'
            ]
        ];
        $this->form->set($fields);
        $this->page_title = 'Delete Project';
        $this->return = 'index.php?url=pjt/delete2';
        $this->view('pjtdel.view.php', ['project' => $project]);
    }

    function delete2()
    {
        $id = $_POST['id'] ?? NULL;
        if (!is_null($id)) {
            $result = $this->projects->delete($id);
            if ($result) {
                emsg('S', 'Project all all releated history deleted.');
            }
            else {
                emsg('F', 'Project NOT deleted.');
            }
        }
        $this->list();
    }

    // show with history
    function hshow($id)
    {
        if (is_null($id)) {
            $this->list();
        }
        $_SESSION['pid'] = $id;
        list($pjt, $isses, $progs) = $this->projects->by_id($id, TRUE);

        $this->page_title = 'Project At-A-Glance';
        $d = [
            'pjt' => $pjt,
            'isses' => $isses,
            'progs' => $progs
        ];
        $this->view('pjtshow.view.php', $d);
    }

    function show($id)
    {
        if (is_null($id)) {
            $this->list();
        }
        $_SESSION['pid'] = $id;
        list($pjt, $isses, $progs) = $this->projects->by_id($id);

        $this->page_title = 'Project At-A-Glance';
        $d = [
            'pjt' => $pjt,
            'isses' => $isses,
            'progs' => $progs
        ];
        $this->view('pjtshow.view.php', $d);
    }
}
